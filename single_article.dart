import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:admob_flutter/admob_flutter.dart';
import 'package:fluentui_icons/fluentui_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:flutterappdemocode/blocs/favArticleBloc.dart';
import 'package:flutterappdemocode/common/ads_helper.dart';
import 'package:flutterappdemocode/common/constants.dart';
import 'package:flutterappdemocode/models/Article.dart';
import 'package:flutterappdemocode/pages/comments.dart';
import 'package:flutterappdemocode/widgets/articleBox.dart';
import 'package:http/http.dart' as http;
import 'package:loading/indicator/ball_beat_indicator.dart';
import 'package:loading/loading.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';

class SingleArticle extends StatefulWidget {
  final dynamic article;
  final String heroId;

  SingleArticle(this.article, this.heroId, {Key key}) : super(key: key);

  @override
  _SingleArticleState createState() => _SingleArticleState();
}

class _SingleArticleState extends State<SingleArticle> {
  List<dynamic> relatedArticles = [];
  Future<List<dynamic>> _futureRelatedArticles;
  final FavArticleBloc favArticleBloc = FavArticleBloc();
  Future<dynamic> favArticle;

  // this variable for frame
  int typeContent = 1;

  @override
  void initState() {
    print(widget.article.content);
    super.initState();

    _futureRelatedArticles = fetchRelatedArticles();

    favArticle = favArticleBloc.getFavArticle(widget.article.id);
  }

  Future<List<dynamic>> fetchRelatedArticles() async {
    try {
      int postId = widget.article.id;
      int catId = widget.article.catId;
      var response = await http.get(Uri.parse(
          "$WORDPRESS_URL/wp-json/wp/v2/posts?exclude=$postId&categories[]=$catId&per_page=3"));

      if (this.mounted) {
        if (response.statusCode == 200) {
          setState(() {
            relatedArticles = json
                .decode(response.body)
                .map((m) => Article.fromJson(m))
                .toList();
          });

          return relatedArticles;
        }
      }
    } on SocketException {
      throw 'No Internet connection';
    }
    return relatedArticles;
  }

  @override
  void dispose() {
    super.dispose();
    relatedArticles = [];
  }

  final ScrollController listController = ScrollController();
  bool _favStatus = false;
  bool isFavorite = false;

  @override
  Widget build(BuildContext context) {
    //   print(widget.article.content);
    final article = widget.article;
    final articleContent = widget.article.content;
    final heroId = widget.heroId;
    if (articleContent.contains("facebook")) {
      typeContent = 2;
    } else {
      typeContent = 1;
    }

    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
            backgroundColor: Color(0xFFB70014),
            iconTheme: IconThemeData(color: Colors.white),
            centerTitle: true,
            title: Image.asset("assets/icon.png", width: 120),
            actions: [
              IconButton(
                  padding: EdgeInsets.only(left: 16),
                  icon: isFavorite
                      ? Icon(Icons.favorite, color: Colors.red)
                      : Icon(Icons.favorite),
                  onPressed: () {
                    favArticleBloc.addFavArticle(article);
                    setState(() {
                      isFavorite = !isFavorite;
                      favArticle = favArticleBloc.getFavArticle(article.id);
                    });
                  })
            ]),
        body: Padding(
          padding: const EdgeInsets.only(bottom: 48),
          child: Stack(
            children: <Widget>[
              Hero(
                  tag: heroId,
                  child: Container(
                      height: (MediaQuery.of(context).size.height / 3) + 33,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: NetworkImage(article.image),
                              fit: BoxFit.cover)))),
              Align(
                alignment: Alignment.topCenter,
                child: ListView(
                  controller: listController,
                  children: <Widget>[
                    SizedBox(
                        height: (MediaQuery.of(context).size.height / 3) - 30),
                    Container(
                      decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(24),
                              topLeft: Radius.circular(24))),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                              child: Text('#' + article.category ?? '',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Theme.of(context).accentColor,
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400))),
                          SizedBox(height: 1),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 8),
                            child: Text(article.title ?? '',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 26)),
                          ),
                          SizedBox(height: 5),
                          Center(
                              child: AdmobBanner(
                                  adUnitId: AdHelper.bannerAdUnitId,
                                  adSize: AdmobBannerSize.MEDIUM_RECTANGLE,
                                  listener: (AdmobAdEvent event,
                                      Map<String, dynamic> args) {},
                                  onBannerCreated:
                                      (AdmobBannerController controller) {})),
                          SizedBox(height: 5),
                          showContent(article.content ?? ''),
                          SizedBox(height: 10),
                          Center(
                              child: AdmobBanner(
                                  adUnitId: AdHelper.bannerAdUnitId,
                                  adSize: AdmobBannerSize.MEDIUM_RECTANGLE,
                                  listener: (AdmobAdEvent event,
                                      Map<String, dynamic> args) {},
                                  onBannerCreated:
                                      (AdmobBannerController controller) {})),
                          SizedBox(height: 10),
                          Padding(
                            padding: EdgeInsets.only(left: 0),
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(0, 10, 0, 5),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Container(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        InkWell(
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        Comments(article.id)));
                                          },
                                          child: Container(
                                            padding: EdgeInsets.only(
                                                top: 0,
                                                bottom: 0,
                                                right: 5,
                                                left: 5),
                                            margin: const EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                right: 5,
                                                left: 5),
                                            width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.50 -
                                                10,
                                            height: 40,
                                            decoration: BoxDecoration(
                                                color: Colors.green,
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                            child: Row(
                                              children: [
                                                IconButton(
                                                  alignment:
                                                      Alignment.bottomCenter,
                                                  icon: Icon(
                                                      FluentSystemIcons
                                                          .ic_fluent_comment_mention_regular,
                                                      color: Colors.white),
                                                  onPressed: () {
                                                    Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                          builder: (context) =>
                                                              Comments(
                                                                  article.id),
                                                        ));
                                                  },
                                                ),
                                                Text(
                                                  "تعليق",
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                        SizedBox(width: 0),
                                        InkWell(
                                          onTap: () {
                                            Share.share('اقرأ هذا الخبر ' +
                                                article.link);
                                          },
                                          child: Container(
                                              width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.50 -
                                                  10,
                                              margin: const EdgeInsets.only(
                                                  top: 5,
                                                  bottom: 5,
                                                  right: 5,
                                                  left: 5),
                                              height: 40,
                                              padding: EdgeInsets.only(
                                                  top: 0,
                                                  bottom: 0,
                                                  right: 5,
                                                  left: 5),
                                              decoration: BoxDecoration(
                                                  color: Colors.yellow[900],
                                                  borderRadius:
                                                      BorderRadius.circular(5)),
                                              child: Row(
                                                children: [
                                                  IconButton(
                                                    icon: Icon(
                                                        FluentSystemIcons
                                                            .ic_fluent_share_regular,
                                                        color: Colors.white),
                                                    onPressed: () {
                                                      Share.share(
                                                          'اقرأ هذا الخبر ' +
                                                              article.link);
                                                    },
                                                  ),
                                                  Center(
                                                      child: Text(
                                                    "مشاركة",
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  ))
                                                ],
                                              )),
                                        ),
                                        SizedBox(width: 0)
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          SizedBox(height: 0),
                          Divider(
                            color: Colors.black,
                          ),
                          Container(
                            child: relatedPosts(_futureRelatedArticles),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget showContent(String article) {

    print(article);
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal:10),
        child: HtmlWidget(
          '''${article.replaceAll('//', 'https://')}''',
          webView: true,
          webViewJs: true,
        ),
      );
  }

  Widget relatedPosts(Future<List<dynamic>> latestArticles) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: FutureBuilder<List<dynamic>>(
        future: latestArticles,
        builder: (context, articleSnapshot) {
          if (articleSnapshot.hasData) {
            if (articleSnapshot.data.length == 0) return Container();

            return Column(
              children: <Widget>[
                Container(
                  alignment: Alignment.topRight,
                  padding:
                      EdgeInsets.only(bottom: 8, right: 16, top: 8, left: 8),
                  child: Text(
                    "مواضيع دات صلة",
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      letterSpacing: .5,
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                Column(
                  children: articleSnapshot.data.map((item) {
                    final heroId = item.id.toString() + "-related";
                    //print('${item.id}');
                    return InkWell(
                      onTap: () {
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => SingleArticle(item, heroId),
                          ),
                        );
                      },
                      child: articleBox(context, item, heroId),
                    );
                  }).toList(),
                ),
                SizedBox(
                  height: 24,
                )
              ],
            );
          } else if (articleSnapshot.hasError) {
            return Container(
                height: 500,
                alignment: Alignment.center,
                child: Text("${articleSnapshot.error}"));
          }
          return Container(
              alignment: Alignment.center,
              width: MediaQuery.of(context).size.width,
              height: 150,
              child: Loading(
                  indicator: BallBeatIndicator(),
                  size: 10.0,
                  color: Theme.of(context).accentColor));
        },
      ),
    );
  }
}
